# Project Instagram

Équipe: Mathieu GOETZ

Suite à un manque de temps seul l'inscription, le login, le post et les likes fonctionnent.

## Attention, les données fournies sont celles données par Chrome dans la section réseau quand on retire la forte latence dû au chargement de nombreuses images venant d'URL externes

Benchmark pour 500 posts (moyenne sur 30): 28ms

Benchmark pour 100 posts (moyenne sur 30): 19ms

Benchmark pour 100 posts (moyenne sur 30): 0.2ms

Benchmark pour du maximum de likes sur 1 seconde: 103