from flask import *
from flask_caching import Cache
from flask_bcrypt import Bcrypt, check_password_hash, generate_password_hash
from google.cloud import datastore
from google.cloud.ndb.exceptions import Error
import datetime
from cerberus import Validator
import time

app = Flask(__name__)
app.secret_key = 'ZfxaM0KXZ6hFfF6inbzisRRHEMac7bezdyklw1yvdug='

bcrypt = Bcrypt(app)
cache = Cache(app, config={'CACHE_TYPE': 'simple'})

datastore_client = datastore.Client(project='mini-insta-295316')

@app.route('/', methods=['GET'])
@cache.cached(timeout=50)
def get_view_feed():
    query = datastore_client.query(kind='Post', order=['-created_at'])
    posts = query.fetch()

    return render_template('feed.html', posts=posts)

@app.route('/view/post/<int:post_id>', methods=['GET'])
def get_view_post(post_id):
    post = datastore_client.get(datastore_client.key('Post', post_id))
    return render_template('post.html', post=post)

@app.route('/insert/post/', methods=['GET'])
def get_insert_post():
    if 'email' not in session:
        return abort(503)
    
    return render_template('insert.html')

@app.route('/insert/post/', methods=['POST'])
def post_insert_post():
    if 'email' not in session:
        return abort(503)

    schema = {
        'title': {
            'type': 'string',
            'minlength': 8,
            'maxlength': 256
        },
        'tags': {
            'type': 'string',
            'minlength': 3,
            'maxlength': 256
        },
        'description': {
            'type': 'string',
            'minlength': 10,
            'maxlength': 1024
        },
        'image': {
            'type': 'string',
            'minlength': 10,
            'maxlength': 512,
            'regex': "(https?:\/\/)?([\w\-])+\.{1}([a-zA-Z]{2,63})([\/\w-]*)*\/?\??([^#\n\r]*)?#?([^\n\r]*)"
        }
    }
    
    validator = Validator(schema, require_all=True)

    if not validator.validate(request.json):
        return jsonify(error=True, messages=errors_format(validator.errors))

    try:
        post = datastore.Entity(datastore_client.key('Post'))
        post.update({
            'title': request.json['title'],
            'tags': request.json['tags'],
            'description': request.json['description'],
            'image_url': request.json['image'],
            'created_at': datetime.datetime.now()
        })
        datastore_client.put(post)

        return jsonify(error=False, messages=['Post saved !'])
    except Error:
        return jsonify(error=True, messages=['Error, could not connect to the database !'])

@app.route('/register/', methods=['GET'])
def get_view_register():
    return render_template('register.html')

@app.route('/register/', methods=['POST'])
def post_insert_register():
    schema = {
        'password': {
            'type': 'string',
            'minlength': 8,
            'maxlength': 256,
            'dependencies': 'repeatPassword'
        },
        'repeatPassword': {
            'type': 'string',
            'minlength': 8,
            'maxlength': 256,
            'dependencies': 'password'
        },
        'email': {
            'type': 'string',
            'regex': '^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$'
        },
        'pseudo': {
            'type': 'string',
            'minlength': 3,
            'maxlength': 32
        }
    }
    
    validator = Validator(schema, require_all=True)
    
    if not validator.validate(request.json):
        return jsonify(error=True, messages=errors_format(validator.errors))
    
    if request.json['password'] != request.json['repeatPassword']:
        return jsonify(error=True, messages=['Error, the two passwords aren\'t identical !'])

    try:
        user_email = datastore_client.get(datastore_client.key('User', request.json['email']))

        if user_email is not None:
            return jsonify(error=True, messages=['Error, user already exists !'])

        user = datastore.Entity(datastore_client.key('User', request.json['email']))
        user.update({
            'email': request.json['email'],
            'pseudo': request.json['pseudo'],
            'password': bcrypt.generate_password_hash(request.json['password']),
            'created_at': datetime.datetime.now(),
            'validated': False
        })
        datastore_client.put(user)

        return jsonify(error=False, messages=['User registered !'])
    except Error:
        return jsonify(error=True, messages=['Error, could not connect to the database !'])

@app.route('/login/', methods=['GET'])
def get_view_login():
    if 'email' in session:
        return redirect(url_for('get_view_feed'))

    return render_template('login.html')

@app.route('/login/', methods=['POST'])
def post_check_login():
    if 'email' in session:
        return redirect(url_for('get_view_feed'))

    try:
        if 'email' not in request.json or not request.json['email']:
            return jsonify(error=True, message='Error, please enter your email address !')

        user = datastore_client.get(datastore_client.key('User', request.json['email']))

        if user is None:
            return jsonify(error=True, message='Error,  no user linked to this email address !')

        if not bcrypt.check_password_hash(user['password'], request.json['password']):
            return jsonify(error=True, message='Error, wrong password !')
            
        session['email'] = request.json['email']
        return jsonify(error=False, message='User logged in !')
    except Error:
        return jsonify(error=True, message='Error, could not connect to the database !')

@app.route('/insert/vote/', methods=['POST'])
def post_insert_vote():
    if 'email' not in session:
        return abort(503)

    try:
        post = datastore_client.get(datastore_client.key('Post', request.json['post_id']))

        if post is not None:
            vote = datastore_client.get(datastore_client.key('Vote', request.json['post_id'], 'Voter', session['email']))

            disliked = False

            if vote is not None:
                datastore_client.delete(datastore_client.key('Vote', request.json['post_id'], 'Voter', session['email']))
                disliked = True
            else:
                vote = datastore.Entity(datastore_client.key('Vote', request.json['post_id'], 'Voter', session['email']))
                vote.update({
                    'created_at': datetime.datetime.now()
                })
                datastore_client.put(vote)

            total_votes = datastore_client.get(datastore_client.key('Vote', request.json['post_id']))

            if total_votes is None and not disliked:
                first_vote = datastore.Entity(datastore_client.key('Vote', request.json['post_id']))
                first_vote.update({
                    'total': 1
                })
                datastore_client.put(first_vote)
                total_votes = {'total': 1}
            else:
                new_vote = 0 if total_votes['total'] is None else total_votes['total']

                if disliked and new_vote != 0:
                    new_vote = new_vote - 1
                else:
                    new_vote = new_vote + 1

                total_votes.update({
                    'total': new_vote
                })
                datastore_client.put(total_votes)
            return jsonify(error=False, message='Vote registered !', post_id=request.json['post_id'], votes=total_votes['total'])
        else:
            return jsonify(error=True, message='Post not found !')
    except Error:
        return jsonify(error=True, message='Error, could not connect to the database !')

@app.route('/select/feed/', methods=['GET'])
def get_select_feed():
    try:
        query = datastore_client.query(kind='Post')
        posts = query.fetch()

        posts_list = []

        for post in posts:
            post_votes = datastore_client.get(datastore_client.key('Vote', post.id))
            user_votes = datastore_client.get(datastore_client.key('Vote', post.id, 'Voter', session['email'])) if 'email' in session else None

            posts_list.append({
                'id': post.id,
                'post_tags': post['tags'],
                'image_url': post['image_url'],
                'post_votes': 0 if post_votes is None else post_votes['total'],
                'post_url': url_for('get_view_post', post_id=post.id),
                'user_liked': user_votes is not None
            })

        return jsonify(error=False, message='Posts selected !', posts=posts_list)
    except Error:
        return jsonify(error=True, message='Error, could not connect to the database !')
    
@app.route('/session/logout/', methods=['GET'])
def get_session_logout():
    if 'email' in session:
        session.pop('email', None)
    
    return redirect(url_for('get_view_feed'))

def errors_format(errors):
    messages = []
    for key, items in errors.items():
        for error in items:
            messages.append(key + ' : ' + error)
    return messages
    
def test_insert_posts(range_number):
    for i in range(range_number):
        post = datastore.Entity(datastore_client.key('Post'))
        post.update({
            'title': 'Titre ' + str(i),
            'tags': '#tag1 #tag2',
            'description': 'Description',
            'image_url': 'https://img-0.journaldunet.com/Hh0mUJt_22a9wqJNMkZ95aMhPSw=/1280x/smart/6b186027188443ddaff495f7df395170/ccmcms-jdn/13265151.jpg',
            'created_at': datetime.datetime.now()
        })
        datastore_client.put(post)

@app.route('/test/like/<int:range_number>', methods=['GET'])
def test_like_posts(range_number):
    start = time.time()
    for i in range(range_number):
        post = datastore_client.get(datastore_client.key('Post', '4656899366584320'))
        if post is not None:
            vote = datastore_client.get(datastore_client.key('Vote', '4656899366584320', 'Voter', 'm.goetz333@laposte.net'))
            disliked = False
            if vote is not None:
                datastore_client.delete(datastore_client.key('Vote', '4656899366584320', 'Voter', 'm.goetz333@laposte.net'))
                disliked = True
            else:
                vote = datastore.Entity(datastore_client.key('Vote', '4656899366584320', 'Voter', 'm.goetz333@laposte.net'))
                vote.update({
                    'created_at': datetime.datetime.now()
                })
                datastore_client.put(vote)
            total_votes = datastore_client.get(datastore_client.key('Vote', '4656899366584320'))
            if total_votes is None and not disliked:
                first_vote = datastore.Entity(datastore_client.key('Vote', '4656899366584320'))
                first_vote.update({
                    'total': 1
                })
                datastore_client.put(first_vote)
                total_votes = {'total': 1}
            else:
                new_vote = 0 if total_votes['total'] is None else total_votes['total']
                if disliked and new_vote != 0:
                    new_vote = new_vote - 1
                else:
                    new_vote = new_vote + 1
                total_votes.update({
                    'total': new_vote
                })
                datastore_client.put(total_votes)

    end = time.time()

    return jsonify(message=str(end - start))